# -*- coding:utf-8 -*-
import base64
import importlib
import aiohttp_jinja2
import jinja2
import common.exceptions
from aiohttp import web
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp_session import setup as session_setup
from common.components import LazyComponent
from common.conf import settings

URLCONF = 'urlpatterns'


class WebApi(object):
    _app: web.Application
    router: web.RouteTableDef

    def __init__(self):
        self._app = web.Application()

        key_ = settings.SECRET_KEY
        if not key_:
            raise common.exceptions.ImproperlyConfigured("SECRET_KEY should be configured")
        if not isinstance(key_, bytes):
            key_ = key_.encode()  # Cast to bytes

        session_setup(self._app,
                      EncryptedCookieStorage(base64.urlsafe_b64decode(key_)))

        aiohttp_jinja2.setup(self._app,
                             loader=jinja2.FileSystemLoader(settings.TEMPLATE_DIR))

        m = importlib.import_module(settings.ROOT_URLCONF)
        patterns = getattr(m, URLCONF, list())
        self._app.add_routes(patterns)

    @property
    def app(self) -> web.Application:
        return self._app

    def serve(self):
        web.run_app(self.app)


class ComponentWebApi(LazyComponent):
    NAME = 'web'

    def _setup(self):
        self._wrapped = WebApi()
