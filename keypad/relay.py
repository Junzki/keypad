# -*- coding:utf-8 -*-
import datetime
import uuid
import dataclasses
import threading
import queue
from typing import Optional, AnyStr


class MessageCounter:

    def __init__(self):
        self._counter = 0
        self._mutex = threading.RLock()

    def next(self) -> int:
        with self._mutex:
            self._counter += 1
            return self._counter

    def current(self) -> int:
        with self._mutex:
            return self._counter


@dataclasses.dataclass
class Message:
    pk: int
    src: str
    dest: str
    payload: AnyStr
    created_at: datetime.datetime = dataclasses.field(default_factory=datetime.datetime.utcnow)
    last_touched: Optional[datetime.datetime] = None


@dataclasses.dataclass
class User:
    pk: str
    username: str
    display_name: str = ''
    created_at: datetime.datetime = dataclasses.field(default_factory=datetime.datetime.utcnow)

    inbox: queue.Queue = dataclasses.field(default_factory=queue.Queue)
    outbox: queue.Queue = dataclasses.field(default_factory=queue.Queue)

    def push(self, m: Message):
        self.outbox.put(m)

    def as_dict(self):
        return {
            'uid': self.pk,
            'username': self.username,
            'display_name': self.display_name,
            'created_at': self.created_at.isoformat()
        }


class Relay:

    def __init__(self):
        self._mutex = threading.RLock()
        self.users = dict()
        self.counter = MessageCounter()

    def list_users(self):
        results = list()
        with self._mutex:
            for user in self.users.values():
                results.append(user.as_dict())

        return results

    def get_user_by_name(self, username: str) -> Optional[User]:
        with self._mutex:
            return self.users.get(username)

    def drop_user(self, username: str):
        with self._mutex:
            self.users.pop(username, None)

    def create_user(self, username: str, display_name: str = '') -> User:
        with self._mutex:
            if username in self.users:
                raise ValueError("User already existed.")

            uid = str(uuid.uuid4())
            user = User(pk=uid,
                        username=username,
                        display_name=display_name or username)
            self.users[username] = user
            return user

    def push(self, src: str, dest: str, payload: AnyStr):
        if dest not in self.users:
            raise ValueError("Dest user does not exist.")

        user = self.users[src]

        s = self.counter.next()
        m = Message(pk=s, src=src, dest=dest, payload=payload)

        user.push(m)


relay = Relay()
