# -*- coding:utf-8 -*-
from aiohttp import web
from .web import (
    index,
    register,
    logout,
    users
)


urlpatterns = [
    web.get('/', index),
    web.get('/users', users),
    web.get('/logout', logout),
    web.post('/register', register)
]
