# -*- coding:utf-8 -*-
import aiohttp_jinja2
from aiohttp import web
from aiohttp_session import get_session
from .relay import relay


@aiohttp_jinja2.template('index.html')
async def index(request):
    session = await get_session(request)
    current_user = session.get('current_user', dict())
    if current_user:
        user = relay.get_user_by_name(current_user.get('username'))
        if not user:
            session.pop('current_user', '')

    context = {
        'current_user': current_user
    }

    return context


async def users(request):
    users_ = relay.list_users()
    return web.json_response(users_)


async def logout(request):
    session = await get_session(request)
    current_user = session.get('current_user', dict())

    if not current_user:
        raise web.HTTPFound('/')

    relay.drop_user(current_user['username'])
    session.pop('current_user')
    return web.Response(headers={
        'Location': '/'
    }, status=web.HTTPFound.status_code)


async def register(request):
    session = await get_session(request)
    body = await request.post()
    username = body['username']
    password = body['password']

    try:
        user = relay.create_user(username)
        uid = user.pk
        err = 'ok'
        status = web.HTTPOk
        session['current_user'] = user.as_dict()
    except ValueError:
        err = 'User existed'
        uid = None
        status = web.HTTPBadRequest

    result = {
        'err': err,
        'user': {
            'uid': uid,
            'username': username
        }
    }

    return web.json_response(data=result,
                             status=web.HTTPFound.status_code,
                             headers={
                                 'Location': '/'
                             })
