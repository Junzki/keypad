# -*- coding:utf-8 -*-
import os
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('command', help='Target action')
parser.add_argument('--settings', '-c', help='Settings module')


if __name__ == '__main__':
    args, argv = parser.parse_known_args()
    cmd = args.command
    conf_ = args.settings
    if not conf_:
        conf_ = 'settings.settings'

    os.environ.setdefault('JOBLET_CONFIG', conf_)
    from keypad.app import holder

    if 'db' == cmd:
        subprocess.run(['alembic', *argv])
        exit(0)
    elif 'shell' == cmd:
        holder.shell()
        exit(0)
    else:
        holder.web.serve()
