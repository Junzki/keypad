# -*- coding:utf-8 -*-
import os
import pathlib
from typing import AnyStr

BASE_DIR = pathlib.Path(__file__).parent.parent

REQUIRED_COMPONENTS = (
    'database',
    'keypad.components.web.ComponentWebApi'
)

SECRET_KEY: AnyStr = '32 bytes size secret key'

DATABASES = {
    'default': {
        'TYPE': 'sqlite',
        'DBNAME': os.path.join(BASE_DIR, 'keypad.db')
    }
}

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
